module.exports = {
	title: "Tramweb",
	url: "https://tramweb.quarante-douze.net/",
	language: "fr",
	description: "Un petit repertoire de site indé francophones",
	author: {
		name: "Kazhnuz",
		email: "kazhnuz@kobold.cafe",
		url: "https://kazhnuz.space/"
	}
}
