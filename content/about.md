---
layout: layouts/base.njk
eleventyNavigation:
  parent: Accueil
  key: À propos
  order: 0
---

{{ metadata.title }} est un site de présentation de site web et créateurs web indépendants (créateurs au sens large) francophone, visant à permettre de découvrir des sujets ou des projets intéressants.

## Pourquoi ?

Il devient plus difficile de faire des recherches et de trouver des nouveaux sites sur les moteurs de recherches traditionnels, et certaines corporations veulent nous faire passer par des IA qui répondront à nos questions plutôt qu'on aille faire des recherches sur des sites. De plus, les réseaux des moteurs sont de plus en plus pollué par des sites généré par IA et optimisé pour le SEO (et donc pour des algorithme plutôt que des humain⋅es).

Plusieurs articles parlent de l'importance qui va grandir de [faire de la curation](https://manuelmoreale.com/curation-search-and-the-future-of-the-web), et ce site est l'un des efforts qui vont exister en ce sens.

L'idée est donc de faire un petit bout de curation, pour le web francophone. Il n'est bien evidemment pas complet, et vise à être en amélioration constante.

## Nan, pourquoi le nom ?

Parce que les trains/trams/metro/transport en commun c'est cool. Tchou tchou 🚅 Et aussi en vrai parce que je trouve que le nom représente bien l'idée "on est dans une gare et on va pouvoir aller vers différents endroits".

## Règles d'inclusion

Les règles d'inclusion sont les suivantes :

- Le site web inclus doit être un site indépendant, et ne pas appartenir à une corporation et/où être une page facebook/twitter/etc.
  - Idem, les wiki ne doivent pas être hébergé sur fandom.com.
- Le site web ne doit pas contenir trop de publicités
- Le site web ne doit pas faire la promotion d'idéologie d'extrème droite, de harcèlement ou tout autre sexisme/racisme/etc.
- Le site web ne doit pas contenir de contenus générés par "IA", ni contenir de contenus volé de manière générale
- Le site web doit être francophone (ou avoir du moins une version francophone)
- Le site web ne doit pas tourner autour des "contenus drama" ou violant la vie privée d'autrui
- Pas de personnes accusées de violence sexuel, grooming, etc

## Possibilités futures

Je réfléchis à plusieurs moyens d'étendre la liste, qui peuvent potentiellement avoir des aventages et des inconvéniants.

- **Rajouter des vidéastes youtube traitant de sujet de manière qualitative**. Le soucis de cette idée est qu'elle ferait à coup sûr la promotion de Youtube d'une manière, et ce site vise à présenter avant tout les sites indépendants. Une possibilité serait de mettre comme règle que le/la vidéaste doit être hébergée au moins de manière informale sur un autre site, mais cela limiterais certaines créatifs indépendants de qualités. Une autre possibilité serait de faire la promotion de l'utilisation d'un frontend alternatif, ou de prévenir quand on a un lien youtube.
- **Rajouter des comptes fedivers intéressants**. Cela diminuerait la démarche "site web" de ce site, mais pourrait permettre aussi aux gens de découvrir les choses cool qui se font sur le fedivers.

## Je ne veux pas être sur cette liste

N'hésitez pas à me contacter du coup, je retirerais sans soucis ! Ce site à pour but de présenter des sites que je trouve intéressant à partager, mais si vous préférez ne pas y être, zéro soucis. Pinguez moi sur les réseaux sociaux, par mail, etc, je ferais ça asap.