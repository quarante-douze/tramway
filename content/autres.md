---
layout: layouts/base.njk
eleventyNavigation:
  key: La gare - Sites similaires
  order: 2
---

La gare recense d'autres projets similaire à celui de {{ metadata.title }}, c'est à dire d'autres travaux de curations qui peuvent exister à travers le web. À noter que contrairement aux *quatiers*, cette liste ne contient pas que des sites francophones, mais aussi des listings anglophones.

## Sites légers

- [1KB Club](https://1kb.club/) - Liste de page faisant 1 kilo-octet ou moins.
- [250KB Club](https://250kb.club/) - Liste de page faisant 256 kilo-octets ou moins.
- [512KB Club](https://512kb.club/) - Liste de page faisant 512 kilo-octets ou moins.
- [1MB Club](https://1mb.club/) - Liste de page faisant 1 mega-octet ou moins.