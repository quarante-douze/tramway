---
layout: layouts/base.njk
eleventyNavigation:
  key: Contribuer au site
  order: 10
---

{{ metadata.title }} est un projet open-source, dont [le code source est disponible sur Codeberg](https://codeberg.org/Kazhnuz/tramweb/) et qui est hébergé par Quarante-Douze. Toutes les contributions sont les bienvenues, tant qu'elles participent à aider à construire un annuaire de site web sain et anti-oppression.

Il y a plusieurs manière de contribuer à la liste et de nous aider à la rendre le mieux possible !

## Ajouter des sites

Ce site étant un site statique dans un dépot git, il est possible d'ajouter des sites via une Merge Request. Pour cela, vous pouvez cloner le répertoire avec git, ajouter les sites dans les catégories où ils doivent aller, et c'est tout bon ! La merge request sera review pour vérifier que [tout les sites conviennent](/about/), et sera merge si c'est bon.

Vous pouvez aussi aller dans le dépots en ligne, et éditer directement un fichier (ceux constituant les pages du sites sont dans `/content/`) en cliquant dessuis puis  sur l'icone de crayons en haut de la visualisation du fichier. Cela ouvrira une interface ou vous pouvez ajouter les sites. Ensuite, une fois vos modifications finie, vous pouvez faire un titre de commit (cela permet de s'y retrouver plus facilement dans l'historique) puis cliquer sur le radio-bouton "Créer une nouvelle branche pour cette révision et initier une demande d'ajout".

A noter que le projet suit le [contributor-covenant](https://www.contributor-covenant.org/) comme code de conduite et que les attaques envers les contributeurs en situation d'oppression (LGBT+, handi, racisées, etc) seront punies.

## Je ne sais pas utiliser git :(

Ce n'est pas grave ! Dans ce cas, il vous suffit de participer en faisant des tickets avec les sites ou créateurs de contenus qui mériteraient selon-vous d'être ajouté à la liste !

Cependant, afin de rendre la vie plus facile aux personnes qui rajouteront le code, il serait cool que vous mettiez déjà en forme votre liste de la manière suivante dans votre message :

<pre>

```markdown
- [NOM_DU_SITE](https://url-du-site-dans-le-navigateur.net) - Description brève du site
- [NOM_DU_SITE](https://url-du-site-dans-le-navigateur.net) - Description brève du site
- [NOM_DU_SITE](https://url-du-site-dans-le-navigateur.net) - Description brève du site
```

</pre>

Cela permettra de juste avoir à copier-coller les sites et les ajouter dans la merge request qu'on fera, ce qui nous permettra de plus vite les ajouter :) C'est un petit peu de temps gagné pour nous, mais cela ferait très plaisir !

## Et sans compte ?

Dans ce cas, vous pouvez aussi contacter kazhnuz via les réseaux sociaux, ou par mail.