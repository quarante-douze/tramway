---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les quartiers
  key: La salle d'arcade
  order: 11
  description: Jeux vidéos, jeux en ligne et jeux de sociétés.
---

Arcadia est le district des jeux vidéos et de tout ce qui est jeux en général. Cela peut être aussi bien pour *parler* de jeux vidéo que pour jouer à des jeux.

## Sites de jeux-vidéo

- [Mag MO5](https://mag.mo5.com/) - Actualité du Retrogaming
- [Romgame](https://www.rom-game.fr/) - Blog parlant de retrogaming, jeu indé et homebrew
- [Emufrance](http://www.emu-france.com/) - Site sur l'émulation
- [Abandonware-France](https://www.abandonware-france.org/) - Site sur les abandonware, ces jeux qui ne sont plus supporté par leurs entreprises d'origine
- [Gros Pixel](https://www.grospixels.com) - Site de jeu retro

## Fansite de jeux

- [Le Monde de Mario](https://www.lemondedemario.fr/) - Fansite Mario
- [Pokébip](https://www.pokebip.com/) - Fansite Pokémon
- [PRAMA Initiative](https://www.prama-initiative.com/) - Site explicatif sur les bugs dans Pokémon
- [Planète-Sonic](https://planete-sonic.com) & [Sonic Online](https://soniconline.fr) - Fansites Sonic
- [SEGA Mag](https://www.sega-mag.com/) - Fansite SEGA
- [Palais de Zelda](https://www.palaiszelda.com/) - Fansite The Legend of Zelda
- [Bravely Oblige](https://www.bravelyoblige.com/) - Fansite Bravely Default
- [Digiduo](https://www.digiduo.fr/) - Fansite digimon
- [Nintendo Town](https://www.nintendo-town.fr/) - Fansite Nintendo
- [Final Fantasy Ring](https://www.ffring.com/) - Fansite Final Fantasy

## Wikis vidéoludiques

> Note : Les wiki fandom ne sont pas présent sur cette liste, à cause des soucis causés par la plateforme Fandom. Le but est d'encourager les wiki utilisant d'autres plateforme ou indépendant.

- [Pokepedia](https://pokepedia.fr) - Wiki Pokémon
- [Wiki Terraria](https://terraria.wiki.gg/fr/wiki/Terraria_Wiki)
- [Wiki Minecraft](https://minecraft.wiki/)
- [Les Archives de Vault-Tec](https://fallout-wiki.com/Accueil) - Wiki Fallout
- [Wiki ARK](https://ark.wiki.gg/fr/wiki/ARK_Wiki)
- [Wiki Dragon Quest](https://wikidragonquest.fr/)

## Jeux en lignes

- [Woltar](https://woltar.net) - Revival d'un jeu en ligne francophone de simulation de vie. Adoptez un woltarien et gérez sa vie et socialisez avec d'autres woltariens.
- [Le mot](https://wordle.louan.me/) - Wordle francophone
- [SUTOM](https://sutom.nocle.fr/) - Jeu de motus en ligne
- [Quordle](https://quordle.desmaths.fr/#/) - Quadruple Wordle
- [Cémantix](https://cemantix.certitudes.org/) - Découvre le mot caché en testant des mots proches
- [Pedantix](https://cemantix.certitudes.org/) - Découvre la page wikipedia cachée en testant des mots
- [WebGMaxiMots](http://gregfresnel.free.fr/WebMotus/WebGMaxiMots.php) - Découvre tout les mots possible avec des lettres
- [Travle](https://travle.earth/) - Cherchez le chemin entre deux pays (ou département)
- [Sémantus](https://www.semantus.fr/) - Proche du Cémantix, contient notamment Sémantus Clap, qui fait deviner un film
- [Piémanto](https://pimiento.janvier.tv/fr) - Quatre jeux de lettres/mots à la Cémantix
- [Webidev](https://webidev.com) - Site de création de simulation d'élevage
- [Tusmo](https://www.tusmo.xyz/) - Wordle multijoueur