---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les quartiers
  key: Le cyberspace
  order: 10
  description: Informatique, technologie et tout ce qui tourne autour.
---

Le cyberspace est le district qui parle de numérique et de technologie en générale ! Ici, vous retrouverez tout ce qui est lié à la bidouille, aux ordinateurs, mais aussi potentiellements aux autres types de matériels.

## Disussions informatiques

- [Un peu de recul](https://unpeuderecul.fr/) - Un peu de recul sur le monde, la société, les technologies, l’environnement, etc
- [Numériquoi ?!](https://numeriquoi.fr/?Accueil) - Comprendre le numérique, pour pouvoir le critiquer et le transformer
- [L'Atelier de Poslovitch](https://blog.poslovitch.fr/) - Explorations autour du logiciel libre, des données ouvertes, du partage et de l'engagement
- [Miscellanées numériques](https://thierryjoffredo.frama.io/)
- [Techsystem.fr](https://techsystem.fr/) - Blog d'un informaticien avec un peu de tech et de linux

## Informatique libre

- [Dégooglisons Internet](https://degooglisons-internet.org/fr/) - Campagne de Framasoft pour des alternatives aux GAFAMs
- [Framasoft](https://framasoft.org/fr/) - Une association d'éducation populaire autour du logiciel libre ([Blog](https://framablog.org/))
- [Collectif CHATONS](https://www.chatons.org/) - Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires.
- [April](https://www.april.org/)- Association de promotion et défense du logiciel libre
- [Quadrature du Net](https://www.laquadrature.net/) - Association combattant pour les libertés fondamentales sur Internet
- [Alternative Numérique](https://alternatives-numeriques.fr/) - Un média pour explorer des alternatives numériques
- [Framalibre](https://framalibre.org/) - Annuaire d'applications libres
- [Fiat Tux](https://fiat-tux.fr/) - Debian, GNU/Linux et geekeries en tout genre

## Internet, webdev/design

- [Index FR IndieWeb](https://indieweb.org/Main_Page-fr)
- [La lutine du web](https://www.lalutineduweb.fr/)
- [24 jours de web](https://www.24joursdeweb.fr/) - Calendrier de l'avant de création web
- [Designers Ethique](https://beta.designersethiques.org/fr) - Association qui explore les pratiques de conception numérique

## Distribution Linux

- [Ubuntu-fr](https://www.ubuntu-fr.org/) - Communauté francophone d'utilisateur d'Ubuntu ([Forums](https://forum.ubuntu-fr.org/))
- [Fedora-fr](https://www.fedora-fr.org/) - Communauté francophone d'utilisateur de Fedora Linux ([Forums](https://forums.fedora-fr.org/))
- [Debian-Facile](https://debian-facile.org/projets:iso-debian-facile) - Site d'entraide pour les nouveaux utilisateurs de Debian
- [Primtux](https://primtux.fr/) - Distribution Linux pour l'école primaire, qui créer aussi le [Libreoffice des Écoles](https://primtux.fr/libreoffice-des-ecoles/)
- [YunoHost](https://yunohost.org/) - Système d'exploitation visant à simplifier autant que possible l'administration d'un serveur pour démocratiser l'auto-hébergement. Communauté principalement francophone. [Page de présentation](https://yunohost.org/fr/overview/what_is_yunohost)

## Rétroinformatique

- [Le grenier du Mac](https://www.grenier-du-mac.net/) - Le site des antiquité Macintosh
- [Retronik](https://retronik.silicium.org/) - Documentation de l'électronique du XXe siècle
- [Silicium](https://silicium.org/) - Association pour la préservation du patrimoine vidéoludique et informatique

## Geekeries diverses

- [Site de Sebsauvage](https://sebsauvage.net/index.html)
- [Blog d'Arthur Perret](https://www.arthurperret.fr/)