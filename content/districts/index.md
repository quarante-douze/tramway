---
layout: layouts/base.njk
eleventyNavigation:
  key: Les quartiers
  order: 1
---

Les quartiers sont le coeur de {{ metadata.title }} (et sont inspiré des districts de Goecities/Neocities districts). Il s'agit des grands types de sujets dans lesquels sont regroupés les sites. 

Les noms sont inspirés (mais ne seront pas forcément repris tel quel) de [la liste des districts](http://www.geocities.ws/NapaValley/2022/cities.html) de Neocities.

## Liste des quartiers

<ul>
  {%- for post in collections.all | sort(false, true, "data.eleventyNavigation.order") -%}
    {%- if post.data.eleventyNavigation.parent == eleventyNavigation.key -%}
      <li><a href="{{ post.url }}">{{ post.data.eleventyNavigation.key }}</a> - {{ post.data.eleventyNavigation.description }} </li>
    {%- endif -%}
  {%- endfor -%}
</ul>