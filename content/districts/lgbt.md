---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les quartiers
  key: Le centre LGBT+
  order: 30
  description: Sites liés aux questions LGBT+
---

Le centre LGBT+ est l'ensemble des sites liés aux questions LGBT+ ! Il a sa propre page plutôt que d'être une sous-partie pour pouvoir regrouper (à noter que cependant, les sites de personnes LGBTs n'y sont pas mis sauf si iels parlent principalement de sujets LGBT+ Genre quelqu'un parlant de cinéma mais qui serait bi sera dans la section cinéma, sauf si son site est spécialisé en "le cinéma et les identités LGBT+").

## Sites d'associations

- [InterLGBT](https://www.inter-lgbt.org/) - Site de l'InterLGBT

## Blogs LGBTs

- [Un genre à soi](https://ungenreasoi.com/) - Blog de bonnes nouvelles par (et pour) des personnes trans
- [Transgrrrls](https://trrransgrrrls.wordpress.com/) - Par des meufs trans, pour les meufs trans
- [Blog de Margot Meunier](https://margotmeunier.wordpress.com/)

## Communautés / forum

- [Forum FTM](https://forum-ftm.fr/) - Forum d'information sur les transidentité masculine FtM/FtX/Ft*

## Wiki et ressources

- [Wikitrans](https://wikitrans.co/) - Resources pour les personnes trans ou en questionnement
- [BDDTrans](https://bddtrans.fr/accueil/) - BDD de practicien⋅ne transfriendly

## Associations

- [Inter-LGBT](https://www.inter-lgbt.org/)
- [Representrans](https://representrans.fr/) - Association visant à changer les réprésentation de la transidentité