---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les quartiers
  key: La médiathèque
  order: 1
  description: Littérature, BD, films et musique.
---

La médiathèque est le district de la création ! Littérature, BD, films, musiques… ici vous pourrez trouver un accès à de nombreuses créations qui peuvent vous intéresser !

## Source de textes

- [Wikisource](https://fr.wikisource.org/wiki/Wikisource:Accueil) - Une bibliothèque de textes libres et gratuits
- [Ebooks Gratuits](www.ebooksgratuits.com/ebooks.php) - Édition de livres numériques du domaine public

## Arts visuels

- [Loki Gwynbleidd](https://lokipropagand.art/) - Visuels anarchistes inspirés par des affiches, cartes et décorations anciennes.

## Site d'auteur⋅ices

- [Lizzie Crowdagger](https://crowdagger.fr/) - Autrice de littérature queer et punk
- [Lionel Davoust](https://lioneldavoust.com/) - Auteur des Dieux sauvages, de Leviathan et Port d'âmes. A créé le podcast procrastination.
- [Andrey Alwett](http://www.audreyalwett.com/) - Autrice du Grimoire d'Elfie et de Magic Charly
- [La Beta Lectrice](https://www.labetalectrice.com/ecrire-un-bon-roman-ca-sapprend/) - Blog parlant de littérature

## Blogs BD & Webcomics

- [Blog de Boulet](https://bouletcorp.com/)
- [Blog de Kek](https://blog.zanorg.com/)
- [Maliki](https://maliki.com/)
- [Grisebouille](https://grisebouille.net/) - Blog BD satirique
- [Les melakarnets](https://melakarnets.com/) - Blog BD de Melaka

## Jeu de rôle

- [AideDD](https://www.aidedd.org/) - Un site sur le jeu de rôle Dungeons & Dragons 5
- [Pathfinder-fr](https://www.pathfinder-fr.org/) - Communauté de fans autour de Pathfinder
- [Chez Whidou](https://www.whidou.fr/) - Fiches de personnages de JDR en français