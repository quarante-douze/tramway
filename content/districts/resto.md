---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les quartiers
  key: Les bars et restaurants
  order: 20
  description: Alimentation et lifestyle.
---

Les bars et restaurants sont le quartier de la nourriture, de l'alimentation et des modes de vies ! Vegetarisme, alimentation, et autres choix pour mener sa vie y sont discuté.

## Végé & vegan

- [Vive la B12](https://www.vivelab12.fr/) - Site d'information sur la consommation de vitamine B12
- [Accidentellement vegan](https://accidentellementvegan.org/) - Recence les produits vegans "par accident"
- [Nantes vegetal](https://nantes-vegetal.fr/) - Les meilleurs adresses vegetariennes/vegan de Nantes
- [France Vegetalienne](https://francevegetalienne.fr/) - Des veganisations de plats traditionnels français

## Cuisine

- [Un jour de soleil](https://www.undejeunerdesoleil.com/) - Site de recettes de cuisine (inspirée de l'italie)