---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les quartiers
  key: L'université
  order: 2
  description: Sciences de la nature et sciences sociales.
---

L'université est le district des sciences de la natures et des sciences sociales. Vous y pourrez trouver de nombreux endroits qui parlent d'informations scientifiques et universitaires.

## Sciences sociales

- [Hacking Sociale](https://www.hacking-social.com/) - Site de psychologie sociale

## Éducation

- [AbulÉdu](https://www.abuledu.org/) - Resources et outils numérique scolaire. Voir aussi le site de l'association [Abuledu-fr](http://abuledu-fr.org/)
  - [Edutwit](https://edutwit.abuledu.org/index.php?r=dashboard%2Fdashboard) - Un twitter-like pour les élèves
- [Edumoov](https://www.edumoov.com/) - Partage de resources éducatives
- [Le livre scolaire](https://www.lelivrescolaire.fr/) - Manuels scolaire fait par des profs
- [Sésamath](https://www.sesamath.net/) - Resources pour l'apprentissage des math
- [Vikidia](https://fr.vikidia.org/wiki/Vikidia:Accueil) - Wiki pour enfant

## Site d'enseignant

- [Lutin Bazar](https://lutinbazar.fr/)
- [Bout de gomme](http://boutdegomme.fr/)
- [Orphée école](http://orpheecole.com/)

## Orthographe

- [Grammalecte](https://grammalecte.net/) - Correcteurs de grammaire pour navigateur/libreoffice
- [Dictionnaire](https://www.le-dictionnaire.com/)
- [Dictionnaire des synonymes](https://www.synonymes.com/)
- [Dictionnaire des conjugaisons](https://www.conjugaison.com/)