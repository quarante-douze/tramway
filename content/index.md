---
layout: layouts/home.njk
eleventyNavigation:
  key: Accueil
  order: 0
---

<header>
  <h1>
    <a href="/" class="home-link">{{ metadata.title }}</a>
  </h1>
</header>

## Bienvenue sur {{ metadata.title }} !

Ce site est un petit annuaire de quelques site web indépendant francophone, conçu dans une optique [IndieWeb](https://indieweb.org/). L'objectif de ce site est de permettre à des gens de potentiellement découvrir des sites qui peuvent les intéresser hors des grosses plateformes d'hébergement de site (les gros réseaux sociaux, fandom, etc). 

Il est dédié à la francophonie, puisque de nombreuses ressources du genre existe pour les anglophone, et que je me suis dit que ce serait bien aussi de permettre de découvrir ce genre de chose pour les personnes ne parlant pas anglais.

Ce site fait partie de [Quarante-Douze](https://quarante-douze.net/)

## Inspiration

Le site est inspiré des défunts géocities et néocities [district](http://www.geocities.ws/NapaValley/2022/cities.html), qui étaient des répertoires de site hébergé sur ces plateformes, triés par thématiques. Le tri et les noms des catégories seront inspiré du concept de District, vous pouvez voir ce site comme le petit tram qui va vous amener dans différentes quartiers d'une grande ville :) 

Le site est aussi inspiré des [Awesome List](https://awesomelists.top/#/) ou [Delightful](https://delightful.club/) dans sa présentation.

## Autres listing

- [Blogroll.fr](https://blogroll.fr/) - Un blogroll de blog francophones

## Crédits

- L'image de fond est [une ancienne carte postale montrant le tram de Nantes](https://commons.wikimedia.org/wiki/File:Artaud-Nozais_741_-_NANTES_-_Un_tramway_%C3%A9lectrique_%C3%A0_la_station_du_haut_de_la_route_de_Rennes.JPG).
- Site généré via [Eleventy](https://www.11ty.dev/)