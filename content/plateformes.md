---
layout: layouts/base.njk
eleventyNavigation:
  key: Plateformes indépendantes
  order: 9
---

Cette liste de site est séparée des autres, parce que le but est de présenter les "plateformes" et services du web indépendants. Tout ce qui est hébergements, moteurs de recherches, etc. à pour but d'être ici. Le but de ce site est de montrer qu'il existe des alternatives au web corporate même en terme de services.

Cette liste, contrairement aux autres, **contient des sites anglphones** puisqu'elle traite de services notamment d'hébergement, et où vous pourrez trouver de nombreux sites.

## Portails et recherche

- [Ytoo!](https://ytoo.org/) - Revival du portail Yahoo
- [Marginalia](https://search.marginalia.nu/) - Moteur de recherche de site indépendant

## Hébergement

- [Neocities](https://neocities.org/) - Hébergeur de site statique à la Geocities
- [Nekoweb](https://nekoweb.org/) - Autre hébergeur de site statiques
- [Yay.boo](https://yay.boo/) - Autre hébergeur de site statiques, par [Good Enough](https://goodenough.us/)
- [Wiki.GG](https://www.wiki.gg/) - Hébergeur de Wiki (alternative à Wikia/Fandom)
- [Miraheze](https://miraheze.org/) - Autre hébergeur de Wiki
- [Bear Blog](https://bearblog.dev/) - Hébergeur de blogs minimalistes
- [Pika Page](https://pika.page/) - Autre hébergeur de blogs minimalistes, par [Good Enough](https://goodenough.us/)
- [Mataora](https://mataroa.blog/) - Autre hébergeur de blogs minimalistes
- [Write.as](https://write.as/) - Hébergeur de blogs dans le fediverse
- [OMG.lol](https://omg.lol/) - Hébergeur fournissant plusieurs fonctionnalités (payant)

## Réseaux sociaux

- [imood](https://imood.com) - Réseau social servant à indiquer son humeur du moment
- [Status.cafe](https://status.cafe) - Réseau sociaux minimaliste
- [Piclog](https://piclog.blue/) - Réseau social de partage d'images fortement compressées
- [Sheezy.art](https://sheezy.art/) - Plateforme de partage d'art à la manière de deviantART (avant Eclipse et le spam IA)
- [Owler](https://owler.cloud/), [Blips](https://blips.club/) et [Pliko](https://pliko.net/) - Revival du Twitter à l'ancienne
- *Fedivers* - Réseau de réseaux sociaux fédérés