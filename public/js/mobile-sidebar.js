document.getElementById('mobile-button').addEventListener('click', function () {
  const sidebar = document.getElementById('sidebar');
  if (!sidebar.classList.contains('shown')) {
    sidebar.classList.remove('hidden');
    sidebar.classList.add('shown');
  } else {
    sidebar.classList.remove('shown');
    sidebar.classList.add('hidden');
  }
});
